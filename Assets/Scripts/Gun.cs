﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {
	GameObject startPoint;
	public GameObject[] projectiles;
	public float speed = 10;
	public static bool canBoat = false;
	private GameObject boat_inner;
	private Rigidbody boatRigidBody;

	public float turnSpeed = 900;
	public float accellerateSpeed = 900;

	// Use this for initialization
	void Start () {
		startPoint = GameObject.Find ("FirePoint");
		boat_inner = GameObject.Find ("boat_inner");
		boatRigidBody = boat_inner.GetComponent<Rigidbody>();

	}

	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Fire2")) {
			int index = Random.Range(0, projectiles.Length);
			GameObject projectile = Instantiate(projectiles[index], startPoint.transform.position + transform.forward*1.03f, startPoint.transform.rotation);
			projectile.GetComponent<Rigidbody>().velocity = transform.TransformDirection (new Vector3 (0, 0, speed));
		}

		if (Input.GetKeyDown(KeyCode.E)){
			float distance = Vector3.Distance(transform.position,boat_inner.transform.position);
			if (canBoat && distance < 3.5) {
				if (!UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl.locked) {
					UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl.locked = true;
					transform.position = boat_inner.GetComponent<Renderer> ().bounds.center;
					transform.parent = boat_inner.transform;
					GetComponent<Rigidbody> ().isKinematic = true;
				} else {
					UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl.locked = false;
					GetComponent<Rigidbody> ().isKinematic = false;
				}
			}
		}

		if (UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl.locked) {
			float h = Input.GetAxis("Horizontal");
			float v = Input.GetAxis("Vertical");

			boatRigidBody.AddTorque(0f,h*turnSpeed*Time.deltaTime,0f);
			boatRigidBody.AddForce(boatRigidBody.transform.forward*v*accellerateSpeed*Time.deltaTime);
		}
	}
}