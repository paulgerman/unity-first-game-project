﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallsDestroyer : MonoBehaviour {
	private int destroyedCount = 0;
	public int maxDestroy = 10;
	void OnCollisionEnter (Collision col)
	{
		if (col.gameObject.layer == 9) {
			this.destroyedCount++;
			if (this.destroyedCount == this.maxDestroy) {
				BallGenerator.spawn = false;

				GameObject wall = GameObject.Find ("WallUp");
				wall.GetComponent<Animation> ().Play ();


				GameObject scene2 = GameObject.Find ("Scene2");
				scene2.GetComponent<Animation> ().Play ();
			}
		}
	}
}
