using UnityEngine;
using System.Collections;

public class OrbitCamera : MonoBehaviour {

	public Transform target;
	public float rotateSpeed = 3.0f;

	float distance;
	float _x;
	float _y;
	float timer;
	float scrollWheel;
	bool colliding = false;
	Vector3 velocity;
	public float smoothTime = 0.3f;

	void Start () {
		distance = 6;
		_y = 45;

		RotateCamera();
		transform.LookAt(target);
		velocity = Vector3.zero;
	}
	
	void Update() {
		
		Quaternion rotation = Quaternion.Euler(_y, _x, 0);
		Vector3 position = rotation * new Vector3(0.0f, 0.0f, -distance) + target.position;
		transform.position = position;


		scrollWheel = Input.GetAxis ("Mouse ScrollWheel");
		
		timer += Time.deltaTime;
		
		if (scrollWheel > 0 && timer > 0.01f) {
			timer = 0;
			distance -= 0.5f;

			if (distance <= 1) {
				distance = 1;
			}

			RotateCamera ();
		}
		
		if (scrollWheel < 0 && timer > 0.01f) {
			timer = 0;
			distance += 0.5f;

			if (distance >= 16) {
				distance = 16;
			}

			RotateCamera ();
		}

		if (Input.GetMouseButton (0)) {
			_x += Input.GetAxis ("Mouse X") * rotateSpeed;
			_y -= Input.GetAxis ("Mouse Y") * rotateSpeed;
			RotateCamera ();
		}

		/*
		if (colliding) {
			transform.localPosition = Vector3.SmoothDamp (transform.localPosition, new Vector3 (0, 1, 0), ref velocity, smoothTime);
		} else {
			RaycastHit hit;
			if (Physics.Raycast(transform.position, -transform.forward, out hit, 1.5f)) {
				print("ray has hit");
			} else {
				transform.localPosition = Vector3.SmoothDamp(transform.localPosition, new Vector3(0, 3.5f, -6), ref velocity, smoothTime);
			}
		}*/
	}

	void RotateCamera() {
		if (_y < -15.15f)
			_y = -15.15f;
		Quaternion rotation = Quaternion.Euler(_y, _x, 0);
		Vector3 position = rotation * new Vector3(0.0f, 0.0f, -distance) + target.position;
		transform.rotation = rotation;
		transform.position = position;
	}

	public void OnTriggerEnter() {
		colliding = true;
	}


	public void OnTriggerExit() {
		colliding = false;
	}
}
