﻿using UnityEngine;
using System.Collections;

public class BallGenerator : MonoBehaviour 
{
	public static bool spawn = true;
	public bool ended = false;
	public bool isSpawning = false;
	public float minTime = 5.0f;
	public float maxTime = 15.0f;
	public GameObject[] enemies;  // Array of enemy prefabs.

	IEnumerator SpawnObject(int index, float seconds)
	{
		yield return new WaitForSeconds(seconds);
		GameObject instance = Instantiate(enemies[index], transform.position + new Vector3(Random.Range(-2.0f, 2.0f),Random.Range(-2.0f, 2.0f),0), transform.rotation);
		instance.GetComponent<Rigidbody> ().AddForce (Random.Range (-1.0f, 1.0f), Random.Range (-1.0f, 1.0f), -5, ForceMode.Impulse);
		//We've spawned, so now we could start another spawn     
		isSpawning = false;
	}

	void Start()
	{
		int enemyIndex = Random.Range(0, enemies.Length);
		StartCoroutine(SpawnObject(enemyIndex, 0));
	}

	void Update () 
	{
		//We only want to spawn one at a time, so make sure we're not already making that call
		if(!isSpawning && spawn)
		{
			isSpawning = true; //Yep, we're going to spawn
			int enemyIndex = Random.Range(0, enemies.Length);
			StartCoroutine(SpawnObject(enemyIndex, Random.Range(minTime, maxTime)));
		}

		//generate balls when spawn ended
		if (!spawn && !ended) {
			ended = true;
			for (int i = 0; i < 20; i++) {
				int index = Random.Range(0, enemies.Length);
				GameObject instance = Instantiate(enemies[index], transform.position + new Vector3(Random.Range(-2.0f, 2.0f),Random.Range(-2.0f, 2.0f),0), transform.rotation);
				instance.GetComponent<Rigidbody> ().AddForce (Random.Range (-1.0f, 1.0f), Random.Range (-1.0f, 1.0f), -5, ForceMode.Impulse);
			}
		}
	}
}