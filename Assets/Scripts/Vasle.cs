﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vasle : MonoBehaviour {
	private static int vasle = 2;
	GameObject character;
	// Use this for initialization
	void Start () {
		character = GameObject.Find ("ThirdPersonController");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject != character)
			return;
		vasle--;
		if (vasle == 0)
			Gun.canBoat = true;
		Destroy(this.gameObject);
	}
}
